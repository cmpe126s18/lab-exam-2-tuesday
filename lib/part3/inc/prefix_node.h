#ifndef CMPE126EXAM1_TUESDAY_PREFIX_NODE_H
#define CMPE126EXAM1_TUESDAY_PREFIX_NODE_H
#include <map>

namespace part3{
    class prefix_node{
    public:
        char value;
        int end;
        std::map<char,prefix_node*> children;

        prefix_node(char input_value);
    };
}

#endif