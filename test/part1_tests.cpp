#include "gtest/gtest.h"

int number_of_2s(int input){
    int count = 0;
    while(input > 0){
        if(input % 10 == 2) count++;
        input = input / 10;
    }
    return count;
}

int number_of_2s_in_range(int start, int end){
    int count = 0;
    while(start <= end){
        count += number_of_2s(start++);
    }
    return count;
}

TEST(Part1, Test1){
    EXPECT_TRUE(true);
}

TEST(Part1, Test2){
    EXPECT_TRUE(true);
}

TEST(Part1, Test3){
    EXPECT_TRUE(true);
}