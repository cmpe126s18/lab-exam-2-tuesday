# Lab Exam 2 #
###### Tuesday, 8 May 2018 ######

Exams that don't compile or can't run all of the submitted tests will get a zero. Comment out or disable tests that are crashing when you run the tests. To disable a test, add `disabled_` to the beginning of the test. Tests that are modified in your exam will not be graded. 

#### Part 1 ####
Write 3 tests to test the functions located in `test/part1_tests.cpp`. Make sure you are thorough in your testing.

#### Part 2 ####
Extend the provided calculator to support the `!` factorial operator.

#### Part 3 ####
Implement a prefix tree using the provided template. 

#### Hints ####
- [Google Test Documentation](https://github.com/google/googletest/tree/master/googletest/docs)
- [Shunting-Yard Algorithm Wiki](https://en.wikipedia.org/wiki/Shunting-yard_algorithm)
- [Prefix Tree Wiki](https://en.wikipedia.org/wiki/Trie)